package com.plugin.detect;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaActivity;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.widget.Toast;
import android.content.Intent;
import java.util.Iterator;
import android.provider.Settings;
import android.net.Uri;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import android.content.IntentSender;
import android.support.v4.app.ActivityCompat;
import android.app.Activity;
import com.plugin.detect.EnableLocationActivity;

/**
 * @author awysocki
 */

public class EnableLocation extends CordovaPlugin{
	public static final String TAG = "detect";

	public static final String REGISTER = "start";
	public static final String UNREGISTER = "stop";
	public static final String EXIT = "exit";
	protected static final int REQUEST_CHECK_SETTINGS = 0x1;
	private static String gECB;
	private static CordovaWebView gWebView;
	protected CallbackContext currentContext;
	private static Status status;


	/**
	 * Gets the application context from cordova's main activity.
	 * @return the application context
	 */
	private Context getApplicationContext() {
		return this.cordova.getActivity().getApplicationContext();
	}
	
	public Activity getActivity(){
		return this.cordova.getActivity();
	}

	@Override
	public boolean execute(String action, JSONArray data, CallbackContext callbackContext) {
		Context context = this.cordova.getActivity();
		currentContext = callbackContext;

		boolean result = false;

		Log.v(TAG, "execute: action=" + action);
		gWebView = this.webView;
		if (REGISTER.equals(action)) {
			try{
				Log.v(TAG, "execute: data=" + data.toString());
				JSONObject jo = data.getJSONObject(0);
				gECB = (String) jo.get("ecb");
				//Toast.makeText(context,data.toString(),Toast.LENGTH_LONG).show();
				displayLocationSettingsRequest(context);
				result = true;
			} catch (JSONException e) {
				Log.e(TAG, "execute: Got JSON Exception " + e.getMessage());
				//Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
				result = false;
				//callbackContext.error(e.getMessage());
			}
			
		} else if (UNREGISTER.equals(action)) {

			Log.v(TAG, "UNREGISTER");
			result = true;
			callbackContext.success();
		} else {
			result = false;
			Log.e(TAG, "Invalid action : " + action);
			callbackContext.error("Invalid action : " + action);
		}

		return result;
		
	}
	
	public static void sendJavascript(JSONObject _json) {
		String _d = "javascript:" + gECB + "(" + _json.toString() + ")";
		Log.v(TAG, "sendJavascript: " + _d);

		if (gECB != null && gWebView != null) {
			gWebView.sendJavascript(_d);
		}
	}
	
	public void displayLocationSettingsRequest(Context context) {
		GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
				.addApi(LocationServices.API).build();
		googleApiClient.connect();

		LocationRequest locationRequest = LocationRequest.create();
		locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		locationRequest.setInterval(10000);
		locationRequest.setFastestInterval(10000 / 2);

		LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
		builder.setAlwaysShow(true);

		PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
		result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
			@Override
			public void onResult(LocationSettingsResult result) {
				status = result.getStatus();
				JSONObject jo = new JSONObject();
				switch (status.getStatusCode()) {
					case LocationSettingsStatusCodes.SUCCESS:
						try{
							jo.put("success","1");
							jo.put("msg","All location settings are satisfied.");
							Log.i(TAG, "All location settings are satisfied.");
							//Toast.makeText(cordova.getActivity(),"All location settings are satisfied.",Toast.LENGTH_LONG).show();
							EnableLocation.sendJavascript(jo);
						} catch (JSONException e) {
							Log.e(TAG, "execute: Got JSON Exception " + e.getMessage());
							//Toast.makeText(cordova.getActivity(),e.getMessage(),Toast.LENGTH_LONG).show();
							//callbackContext.error(e.getMessage());
						}
						break;
					case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
						Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
						//Toast.makeText(cordova.getActivity(),"Location settings are not satisfied. Show the user a dialog to upgrade location settings ",Toast.LENGTH_LONG).show();
						try{
							cordova.getActivity().startActivity(new Intent(cordova.getActivity(), EnableLocationActivity.class));
						}catch(Exception e){
							//Toast.makeText(cordova.getActivity(),e.getMessage(),Toast.LENGTH_LONG).show();
						}
						//EnableLocation.sendJavascript(jo);
						break;
					case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
						try{
							Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
							//Toast.makeText(cordova.getActivity(),"Location settings are inadequate, and cannot be fixed here. Dialog not created.",Toast.LENGTH_LONG).show();
							jo.put("success","0");
							jo.put("msg","Location settings are inadequate, and cannot be fixed here. Dialog not created.");
						}catch (JSONException e) {
							Log.e(TAG, "execute: Got JSON Exception " + e.getMessage());
							//Toast.makeText(cordova.getActivity(),e.getMessage(),Toast.LENGTH_LONG).show();
							try{
								jo.put("success","0");
								jo.put("msg",e.getMessage());
							}catch(Exception e1){
							}
						}
						EnableLocation.sendJavascript(jo);
						break;
				}
			}
		});
	}

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

	@Override
    public void onPause(boolean multitasking) {
        super.onPause(multitasking);
    }

    @Override
    public void onResume(boolean multitasking) {
        super.onResume(multitasking);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
	
	public static void setActivityReference(EnableLocationActivity act1) {
		try {
			status.startResolutionForResult(act1, REQUEST_CHECK_SETTINGS);
		}
		catch (IntentSender.SendIntentException e) {
			Log.i(TAG, "PendingIntent unable to execute request.");
			//Toast.makeText(act1,"PendingIntent unable to execute request.",Toast.LENGTH_LONG).show();
			/*jo.put("success","0");
			EnableLocation.sendJavascript(jo);*/
		}
	}
}
