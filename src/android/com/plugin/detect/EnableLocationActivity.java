package com.plugin.detect;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;
import com.plugin.detect.EnableLocation;
import android.os.Bundle;
import org.json.JSONObject;
import org.json.JSONException;

public class EnableLocationActivity extends Activity{

	protected static final int REQUEST_CHECK_SETTINGS = 0x1;

	@Override
	public void onCreate(Bundle sis) {
		super.onCreate(sis);
		EnableLocation.setActivityReference(this);
	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode,resultCode,data);
		JSONObject jo = new JSONObject();
		try{
			//Toast.makeText(this,"Found",Toast.LENGTH_LONG).show();
			switch (requestCode) {
				case REQUEST_CHECK_SETTINGS:
					switch (resultCode) {
						case Activity.RESULT_OK:
							jo.put("success","1");
							jo.put("msg","success");
						 break;
						case Activity.RESULT_CANCELED:
							jo.put("success","0");
							jo.put("msg","Rejected");
						 break;
						default:
							jo.put("success","1");
							jo.put("msg","success");
						 break;
					}
					break;
			}
		}catch (JSONException e) {
		}catch(Exception e){
			try{
				jo.put("success","0");
				jo.put("msg",e.getMessage());
			}catch(JSONException ee){}
		}
		EnableLocation.sendJavascript(jo);
		finish();
        /*if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() && !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }*/
    }
}