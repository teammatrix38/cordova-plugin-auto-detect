var detect=function(){
};

detect.prototype.enableLocation = function(options){	
	cordova.exec(function(success){}, function(error){}, "EnableLocation", "start",[options]);
}

if(!window.plugins) {
    window.plugins = {};
}
if (!window.plugins.detect) {
    window.plugins.detect = new detect();
}

if (typeof module != 'undefined' && module.exports) {
  module.exports = detect;
}